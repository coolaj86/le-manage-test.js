'use strict';

//var tester = require('le-manage-test');
var tester = require('./');

// This could be implemented in memory, with a database, with the file system, etc
var db = {};
function searchDb(term) {
  var info = db[term];
  // find either the info, or the relation to the info
  // { subject: ..., altnames: [...] }
  // { related: ... } // related is the key
  if (info && info.related) {
    info = db[info.related];
  }
  return info || null;
}

// The function that checks the database for the domain (or its wildcard) and returns the results
function approveDomains(opts) {
  // foo.example.com
  var domain = opts.domain;
  // foo.example.com => *.example.com
  var wildname = opts.wildname;

  // try an exact match
  var info = searchDb(domain);
  // try the wildcard
  if (!info) {
    info = searchDb(wildname);
  }

  // If there's no info, it didn't exist, return null (not undefined)
  if (!info) { return null; }

  //return { subject: 'example.com', altnames: [ 'example.com', 'www.example.com' ] };
  return { subject: info.subject, altnames: info.altnames };
}

function updateDomains(opts) {
  // Set the subject
  //db['example.com'] = { subject: 'example.com', altnames: [ 'example.com', 'www.example.com' ] };
  db[opts.subject] = { subject: opts.subject, altnames: opts.altnames };

  opts.altnames.forEach(function (altname) {
    // Don't overwrite the subject
    if (altname === opts.subject) { return; }

    // Set a relation for each altname
    //db['www.example.com'] = { related: 'example.com' };
    db[altname] = { related: opts.subject };
  });

  // return null (not undefined)
  return null;
}

tester.test({
  set: updateDomains
, get: approveDomains
}).then(function () {
  console.info("PASS");
});
