# [le-manage-test](https://git.coolaj86.com/coolaj86/le-manage-test.js.git)

| A [Root](https://rootprojects.org) Project |

The test harness you should use when writing a management strategy
for [Greenlock](https://git.coolaj86.com/coolaj86/greenlock-express.js) v2.7+ (and v3).

All implementations that support multiple domains MUST pass these tests
(which is not a hard thing to do).

## Install

```bash
npm install --save-dev le-manage-test@3.x
```

## Usage

```js
var tester = require('le-manage-test');

tester.test({
  set: function updateDomains(info) {
    // { subject: 'example.com'
    // , altnames: ['example.com', '*.example.com', 'foo.bar.example.com' ] }
    DB.set(...)
    return null;
  }
, get: function approveDomains(query) {
    // { domain: 'www.example.com'
    // , wildname: '*.example.com' // (for convenience, if you need it)
    return DB.get(...).then(function () {
      // { subject: 'example.com', altnames: [...] }
      return info;
    });
  }
}).then(function () {
  console.info("PASS");
});
```

Note: The management plugin and storage plugins must support wildcards,
but if the user can't select or implement a dns-01 challenge then that
user simply doesn't get to use them. No worries. Nothing breaks.

## Overview

Here's a more expanded breakdown of what the implementations might look like
(if that was too terse above):

```js
var tester = require('le-manage-test');
```

```js
// The function that checks the database for the domain (or its wildcard) and returns the results
function approveDomains(opts) {
  var domain = opts.domain;

  // try exact match (ex: www.example.com)
  var info = DB.find(domain);

  // try wildcard match (ex: *.example.com)
  if (!info) { info = DB.find(wild) }

  // If there's no info, it didn't exist, return null (not undefined)
  if (!info) { return null; }

  //return { subject: 'example.com', altnames: [ 'example.com', 'www.example.com' ] };
  return { subject: info.subject, altnames: info.altnames };
}
```

```js
function updateDomains(opts) {
  // return null (not undefined)
  return DB.associate(opts.subject, opts.altnames);
}
```

```js
tester.test({
  set: updateDomains
, get: approveDomains
}).then(function () {
  console.info("PASS");
});
```

## Example

See `example.js`.

Will post reference implementations here later...
