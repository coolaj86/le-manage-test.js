'use strict';
/*global Promise*/

// This same test must work for every implementation
var fixtures = [
  { _input: 'foo.example.com'
  , subject: 'foo.example.com'
  , altnames: [ 'foo.example.com', '*.foo.example.com' ]
  }
, { _input: 'foo.bar.example.com'
  , subject: 'bar.example.com'
  , altnames: [ 'bar.example.com', '*.bar.example.com' ]
  }
, { _input: 'foo.baz.example.xyz'
  , subject: 'bar.baz.example.net'
  , altnames: [ 'bar.baz.example.net', 'foo.baz.example.xyz', '*.baz.example.com' ]
  }
, { _input: 'gralt.example.xyz'
  , subject: null
  , altnames: null
  }
];

// foo.example.com => *.example.com
function untame(str) {
  return str.split('.').slice(1).join('.');
}

function compare(input, out) {
  var err;

  if (!input.subject && !out) {
    return true;
  }
  if (!input.subject && out && (out.subject || out.altnames)) {
    throw new Error("should not have returned results for query for '" + input._input + "', but got "
      + JSON.stringify(out));
  }

  err = new Error("The query for '" + input._input + "' should have been found at or through '"
      + input.subject + "' and should have had the altnames '" + input.altnames + "', but instead got "
      + JSON.stringify(out));
  if (input.subject && (!out || !out.subject || !Array.isArray(out.altnames))) {
    throw err;
  }
  if (input.subject !== out.subject) {
    throw err;
  }
  if (input.subject !== out.altnames[0]) {
    throw new Error("The subject '" + input.subject
      + "' MUST be the first element altnames '" + out.altnames.join(' ') + "'.");
  }
  if (input.altnames.join('|') !== out.altnames.join('|')) {
    throw err;
  }

  return true;
}

function forEach(arr, fn) {
  function next() {
    var el = arr.shift();
    if (!el) { return Promise.resolve(); }
    return Promise.resolve().then(function () {
      return fn(el);
    }).then(next);
  }
  return Promise.resolve().then(next);
}

module.exports.create = function () {
  throw new Error("le-manage-test is a test fixture for le-manage-* plugins, not a plugin itself");
};

module.exports.test = function (manager) {
  // Binding to 'this', just in case that's needed
  var set = manager.set.bind(manager);
  var get = (manager.get|| manager.approveDomains).bind(manager);

  // call the updateDomains function for each of the fixtures,
  // setting up the database (or object or filesystem or whatever).
  function setup() {
    var arr = fixtures.slice(0);
    return forEach(arr, function (el) {
      // don't try to save the absent things (for the negative tests)
      if (el.subject) {
        set(el);
      }
      return null;
    });
  }

  // call the approveDomains function for each of the fixtures
  // and see if we get back exactly the response we expect
  function run() {
    var arr = fixtures.slice(0);
    return forEach(arr, function (el) {
      return Promise.resolve(get({ domain: el._input, wildname: untame(el._input), domains: [ el._input ] })).then(function (out) {
        if (!compare(el, out)) {
          throw new Error("did not return true (internal test error)");
        }
      });
    });
  }

  return setup().then(run);
};
